﻿using System;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;
using System.IO;
using System.Reflection;

namespace CFDI33
{
    public partial class FSAT : DevExpress.XtraEditors.XtraForm
    {
        public bool vProcesoAutomatico;
        public FSAT()
        {
            InitializeComponent();
        }

        private void FSAT_Load(object sender, EventArgs e)
        {
            progress.Visible = false;
            if (Negocio.CNegocio.leerValorConfiguracion("SATAuto") == "TRUE")
            {
                if (vProcesoAutomatico)
                {
                    txtEmisor.Text = Negocio.CNegocio.GetEntradConfig("RFCReceptor");
                    txtReceptor.Text = "";
                    dateInicio.DateTime = DateTime.Today.AddDays(-1);
                    dateFin.DateTime = DateTime.Now;
                    System.Threading.Thread.Sleep(3000);
                    Descarga("Emisor");
                    txtLog.Text += DateTime.Now + "Inicia Procesos Automatico Espere..."  + "\r\n";
                    System.Threading.Thread.Sleep(10000);
                    txtEmisor.Text = "";
                    txtReceptor.Text = Negocio.CNegocio.GetEntradConfig("RFCReceptor"); 
                    dateInicio.DateTime = DateTime.Today.AddDays(-1);
                    dateFin.DateTime = DateTime.Now;
                    System.Threading.Thread.Sleep(3000);
                    Descarga("Emisor");
                    txtLog.Text += DateTime.Now + "Inicia Procesos Automatico Espere..." + "\r\n";
                }
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                Application.DoEvents();
                if (Negocio.CNegocio.vProcesando ==  false)
                    if (Descarga("Libre"));
            }
            catch(Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                MessageBox.Show("Error al intentar descargar "+Ex.Message, "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                progress.Visible = false;
            }
        }

       private bool Descarga(String vTipo)
        {
            bool Result = false;
            WaitForm waitForm = new WaitForm();
            SplashScreenManager.ShowForm(this, typeof(WaitForm), true, true, false);
            Negocio.CNegocio.EscribeLog("Peticion al SAT: "+ txtEmisor.Text+", "+txtReceptor.Text + ", " + dateInicio.DateTime.ToString("yyyy-MM-dd") + ", " +dateFin.DateTime.ToString("yyyy-MM-dd"));
            Result = Negocio.CNegocio.DescargaSAT(txtEmisor.Text, txtReceptor.Text, dateInicio.DateTime.ToString("yyyy-MM-dd"), dateFin.DateTime.ToString("yyyy-MM-dd"), txtLog, waitForm, Convert.ToInt16(vProcesoAutomatico), vTipo, dateInicio.DateTime.ToString("yyyy-MM-dd")+"-"+ dateFin.DateTime.ToString("yyyy-MM-dd"));
            SplashScreenManager.CloseForm(false);
            return Result;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            WaitForm waitForm = new WaitForm();
            SplashScreenManager.ShowForm(this, typeof(WaitForm), true, true, false);
            if (Negocio.CNegocio.vProcesando == false)
                Negocio.CNegocio.MonitorSolicitud(txtLog, waitForm);
            SplashScreenManager.CloseForm(false);
        }

        private void txtLog_TextChanged(object sender, EventArgs e)
        {
            txtLog.Focus();
            txtLog.Select(txtLog.Text.Length, 0);
            txtLog.ScrollToCaret();

        }

        private void ProcesoDescarga()
        {
            Negocio.CNegocio.EscribeLog("Inicia Procesos Automatico");
            WaitForm waitForm = new WaitForm();
            txtLog.Text += DateTime.Now +"Inicia Procesos Automatico"+ "\r\n";
            SplashScreenManager.ShowForm(this, typeof(WaitForm), true, true, false);
            Negocio.CNegocio.MonitorSolicitud(txtLog, waitForm);
            SplashScreenManager.CloseForm(false);
        }

        private void tProceso_Tick(object sender, EventArgs e)
        {
            if (Negocio.CNegocio.vProcesando == false)
                ProcesoDescarga();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WaitForm waitForm = new WaitForm();
            SplashScreenManager.ShowForm(this, typeof(WaitForm), true, true, false);
            if (Negocio.CNegocio.vProcesando == false)
                Negocio.CNegocio.simulaProceso(txtLog, waitForm, "0E31B595-E354-4A0D-9EB9-52B3043162A9_01");
            SplashScreenManager.CloseForm(false);
        }
    }
}