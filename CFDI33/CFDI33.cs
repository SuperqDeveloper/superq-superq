﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraSplashScreen;

namespace CFDI33
{
    public partial class CFDI33 : DevExpress.XtraEditors.XtraForm
    {
        public CFDI33()
        {
            InitializeComponent();
        }

        private void CFDI33_Shown(object sender, EventArgs e)
        {
            //dateInicio.DateTime = DateTime.Now;
            //dateInicio.DateTime = DateTime.Now;
            try
            {
                CargaDatos("", "", "", 0);
                status.Items[0].Text = gvCFDI.RowCount + " Registros Encontrados";
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
            }

        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            WaitForm waitForm = new WaitForm();
            SplashScreenManager.ShowForm(this, typeof(WaitForm), true, true, false);
            Negocio.CNegocio.vProcesando = true;
            if (Negocio.CNegocio.RecoreCarpeta("\\CFDI\\Aprocesar", waitForm))
            {
                Negocio.CNegocio.EscribeLog("Importardando CDFI a DB");
                CargaDatos("", "", "", 0);
                //SplashScreenManager.Default.SetWaitFormDescription(" " );
                MessageBox.Show("Archivos Guardados", "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            status.Items[0].Text = "";
            Negocio.CNegocio.vProcesando = false;
            SplashScreenManager.CloseForm(false);
        }

        private bool CargaDatos(string vRFC, string vFechaIni, string vFechaFin, Int64 vCFDI)
        {
            string except = "";
            try
            {
                gCFDI.DataSource = Negocio.CNegocio.BuscaCDFI(out except, 1, "", "", "", "", "", "", "", "", "", "", 0, 0, "", 0, "", "", "", "", "", vRFC, vFechaIni, vFechaFin, vCFDI, "");
                CheckMinimumWidth(gvCFDI);
                if (gvCFDI.RowCount < 1)
                    return false;
                else
                    return true;
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                MessageBox.Show("Error: Al Buscar CFDI'S" + except + " " + Ex.Message, "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return false;
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {

            try
            {
                if (gvCFDI.DataRowCount > 0)
                {
                    if (saveFile.ShowDialog() == DialogResult.OK)
                    {
                        if (saveFile.FileName != "")
                        {
                            if (ExportExcel(saveFile.FileName))
                            {
                                Negocio.CNegocio.EscribeLog("Exportando a Excel");
                                MessageBox.Show("Reporte Creado ", "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                                Process.Start(@saveFile.FileName);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Ingrese Un Nombre de Archivo", "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
            }
        }

        private bool ExportExcel(string fileName)
        {
            try
            {
                gCFDI.ExportToXlsx(fileName);
                return true;
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                return false;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string except = "";
            try
            {
                Negocio.CNegocio.EscribeLog("Busca En DB " + txtRfcEmi.Text + ", " + txtNomEmi.Text + ", " + txtSerie.Text + ", " + txtFolio.Text + ", " + dateInicio.Text + ", " + dateFin.Text);
                gCFDI.DataSource = Negocio.CNegocio.SQSP_BUSCACFDISSP(out except, txtRfcEmi.Text, txtNomEmi.Text, txtSerie.Text, txtFolio.Text, dateInicio.Text, dateFin.Text);
                status.Items[0].Text = gvCFDI.RowCount + " Registros Encontrados";
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                MessageBox.Show("No Se Encontraron Registros Con estos Criterios " + "\n" + except, "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }

        private void CheckMinimumWidth(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            GridViewInfo viewInfo = (GridViewInfo)view.GetViewInfo();
            if (viewInfo.ColumnsInfo.LastColumnInfo.Bounds.Right > viewInfo.ViewRects.ColumnPanel.Right)
            {
                view.OptionsView.ColumnAutoWidth = false;
                view.BestFitColumns();
            }
            else view.OptionsView.ColumnAutoWidth = true;
        }

        private void gCFDI_DoubleClick_1(object sender, EventArgs e)
        {
            FConceptos fConceptos = new FConceptos();
            fConceptos.vCFDI = Convert.ToInt64(gvCFDI.GetRowCellValue(gvCFDI.FocusedRowHandle, "IDCFDI").ToString());
            fConceptos.ShowDialog();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            FSAT fSAT = new FSAT();
            fSAT.vProcesoAutomatico = false;
            fSAT.ShowDialog();
        }

        private void tAuto_Tick(object sender, EventArgs e)
        {
            if (Negocio.CNegocio.leerValorConfiguracion("HoraProcesamiento") == DateTime.Now.ToString("HH:mm"))
            {
                if (Negocio.CNegocio.leerValorConfiguracion("SATAuto") == "TRUE")
                {
                    ProcesoAutomatico();
                }
            }
        }

        private void ProcesoAutomatico()
        {
            Form existe = Application.OpenForms.OfType<FSAT>().Where(pre => pre.Name == "FSAT").SingleOrDefault<FSAT>();
            //Application.OpenForms.OfType<FSAT>().Where(pre => pre.Name == "FSAT").SingleOrDefault<Form>();
            if (existe == null)
            {
                FSAT fSAT = new FSAT();
                fSAT.vProcesoAutomatico = true;
                fSAT.ShowDialog();
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            WaitForm waitForm = new WaitForm();
            SplashScreenManager.ShowForm(this, typeof(WaitForm), true, true, false);
            Negocio.CNegocio.vProcesando = true;
            if (Negocio.CNegocio.RecoreCarpetaMeta("\\CFDI\\Aprocesar", waitForm))
            {
                Negocio.CNegocio.EscribeLog("Importardando CDFI a DB");
                //CargaDatos("", "", "", 0);
                //SplashScreenManager.Default.SetWaitFormDescription(" " );
                MessageBox.Show("Archivos Guardados", "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            status.Items[0].Text = "";
            Negocio.CNegocio.vProcesando = false;
            SplashScreenManager.CloseForm(false);
        }
    }
}


