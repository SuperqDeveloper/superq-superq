﻿namespace CFDI33
{
    partial class WaitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progress = new DevExpress.XtraWaitForm.ProgressPanel();
            this.progress1 = new System.Windows.Forms.TableLayoutPanel();
            this.progress1.SuspendLayout();
            this.SuspendLayout();
            // 
            // progress
            // 
            this.progress.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.progress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progress.Appearance.Options.UseBackColor = true;
            this.progress.AppearanceCaption.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progress.AppearanceCaption.Options.UseFont = true;
            this.progress.AppearanceDescription.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progress.AppearanceDescription.Options.UseFont = true;
            this.progress.BarAnimationElementThickness = 2;
            this.progress.Caption = "Espere. Procesando";
            this.progress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progress.ImageHorzOffset = 20;
            this.progress.Location = new System.Drawing.Point(0, 17);
            this.progress.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(900, 144);
            this.progress.TabIndex = 0;
            this.progress.Text = "progressPanel1";
            this.progress.UseWaitCursor = true;
            // 
            // progress1
            // 
            this.progress1.AutoSize = true;
            this.progress1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.progress1.BackColor = System.Drawing.Color.Transparent;
            this.progress1.ColumnCount = 1;
            this.progress1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.progress1.Controls.Add(this.progress, 0, 0);
            this.progress1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progress1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.progress1.Location = new System.Drawing.Point(0, 0);
            this.progress1.Name = "progress1";
            this.progress1.Padding = new System.Windows.Forms.Padding(0, 14, 0, 14);
            this.progress1.RowCount = 1;
            this.progress1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.progress1.Size = new System.Drawing.Size(900, 178);
            this.progress1.TabIndex = 1;
            // 
            // WaitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(900, 178);
            this.Controls.Add(this.progress1);
            this.DoubleBuffered = true;
            this.Name = "WaitForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Form1";
            this.progress1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public DevExpress.XtraWaitForm.ProgressPanel progress;
        public System.Windows.Forms.TableLayoutPanel progress1;
    }
}
