﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;

namespace Datos
{
    public class AccesoDatos
    {
        public static void EscribeLog(string vLinea, string ruta)
        {
            try
            {
                string file;
                file = ruta + "\\Log\\" + Getfecha() + ".log";
                Creacarpeta(ruta + "\\Log");
                using (StreamWriter write = new StreamWriter(file, true))
                {
                    write.WriteLine(vLinea + ";" + Gethorafecha() + ";");
                    write.Close();
                }
            }
            catch (Exception Ex)
            {

                EscribeLog(Ex.Message, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                //MessageBox.Show("Error: Al Buscar CFDI'S" + " " + Ex.Message, "SUPERQ", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }

        public static string Getfecha()
        {
            return (DateTime.Now.ToString("ddMMyyyy"));
        }

        public static string Gethorafecha()
        {
            return DateTime.Now.ToString("ddMMyyyy HH:mm:ss");
        }

        public static void Creacarpeta(string ruta)
        {
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
        }


        public static DataTable PCFDI(out string excep, int vOpcion, string vRFCEmisor, string vNombreEmisor, string vRegimenFiscal, string vRFCReceptor, string vNombreReceptor, string vUsoCFDI, string vSerie, string vFolio, string vFecha,
            string vFormaPago, double vSubtotal, double vTotal, string vMoneda, double vTipoCambio, string vTipoDeComprobante, string vMetodoPago, string vLugarExpedicion, string vUUID, string vFechaTimbrado, string vRFC, string vFechaIni, string vFechaFin, Int64 vIDCFDI, string vArchivo, string vXml)
        {
            DataTable resultado = new DataTable();
            SqlCommand _comando = Configuracion.CrearComandoProc("");
            _comando.CommandText = "PCFDI;1";
            SqlParameter[] pParametros = new SqlParameter[]{
                new SqlParameter("@Opcion", vOpcion),
                 new SqlParameter("@RFCEmisor", vRFCEmisor),
                 new SqlParameter("@NombreEmisor", vNombreEmisor),
                 new SqlParameter("@RegimenFiscal", vRegimenFiscal),
                 new SqlParameter("@RFCReceptor", vRFCReceptor),
                 new SqlParameter("@NombreReceptor", vNombreReceptor),
                 new SqlParameter("@UsoCFDI", vUsoCFDI),
                 new SqlParameter("@Serie", vSerie),
                 new SqlParameter("@Folio", vFolio),
                 new SqlParameter("@Fecha", vFecha),
                 new SqlParameter("@FormaPago", vFormaPago),
                 new SqlParameter("@Subtotal", vSubtotal),
                 new SqlParameter("@Total", vTotal),
                 new SqlParameter("@Moneda", vMoneda),
                 new SqlParameter("@TipoCambio", vTipoCambio),
                 new SqlParameter("@TipoDeComprobante", vTipoDeComprobante),
                 new SqlParameter("@MetodoPago", vMetodoPago),
                 new SqlParameter("@LugarExpedicion", vLugarExpedicion),
                 new SqlParameter("@UUID", vUUID),
                 new SqlParameter("@FechaTimbrado", vFechaTimbrado),
                 new SqlParameter("@RFC", vRFC),
                 new SqlParameter("@FechaIni", vFechaIni),
                 new SqlParameter("@FechaFin", vFechaFin),
                 new SqlParameter("@IDCFDI", vIDCFDI),
                 new SqlParameter("@Archivo", vArchivo),
                 new SqlParameter("@xml", vXml),
            };
            return Configuracion.EjecutarProcedure(_comando, pParametros, out excep);
        }


        public static DataTable PCFDIConceptos(out string excep, int vOpcion, Int64 vIDConcepto, Int64 vIDCFDI, string vClaveProdServ, string vNoIdentificacion, double vCantidad, string vClaveUnidad,
            string vDescripcion, double vValorUnitario, double vImporte)
        {
            DataTable resultado = new DataTable();
            SqlCommand _comando = Configuracion.CrearComandoProc("");
            _comando.CommandText = "PCFDIConceptos;1";
            SqlParameter[] pParametros = new SqlParameter[]{
                 new SqlParameter("@Opcion", vOpcion),
                 new SqlParameter("@IDConcepto", vIDConcepto),
                 new SqlParameter("@IDCFDI", vIDCFDI),
                 new SqlParameter("@ClaveProdServ", vClaveProdServ),
                 new SqlParameter("@NoIdentificacion", vNoIdentificacion),
                 new SqlParameter("@Cantidad", vCantidad),
                 new SqlParameter("@ClaveUnidad", vClaveUnidad),
                 new SqlParameter("@Descripcion", vDescripcion),
                 new SqlParameter("@ValorUnitario", vValorUnitario),
                 new SqlParameter("@Importe", vImporte),
            };
            return Configuracion.EjecutarProcedure(_comando, pParametros, out excep);
        }

        public static DataTable PCFDImpuestos( out string except, int vOpcion, Int64 vIDConcepto,  double vBase, double vImporte, string vImpuesto, double vTasaOCuota, string vTipoFactor)
        {
            SqlCommand _comando = Configuracion.CrearComandoProc("");
            _comando.CommandText = "PCFDIImpuestos;1";
            SqlParameter[] pParametros = new SqlParameter[]{
                 new SqlParameter("@Opcion", vOpcion),
                 new SqlParameter("@IDConcepto", vIDConcepto),
                 new SqlParameter("@Base", vBase),
                 new SqlParameter("@Importe", vImporte),
                 new SqlParameter("@Impuesto", vImpuesto),
                 new SqlParameter("@TasaOCuota", vTasaOCuota),
                 new SqlParameter("@TipoFactor", vTipoFactor),
      
            };
            return Configuracion.EjecutarProcedure(_comando, pParametros, out except);

        }

        public static DataTable PBUSCACFDISSP(out string except, string vRfc, string vNombre, string vSerie, string vFolio, string vFechaInicio, string vFechaFin)
        {
            SqlCommand _comando = Configuracion.CrearComandoProc("");
            _comando.CommandText = "PBUSCACFDISSP;1";
            SqlParameter[] pParametros = new SqlParameter[]{
                 new SqlParameter("@RFC", vRfc),
                 new SqlParameter("@NOMBRE", vNombre),
                 new SqlParameter("@SERIE", vSerie),
                 new SqlParameter("@FOLIO", vFolio),
                 new SqlParameter("@FECHAI", vFechaInicio),
                 new SqlParameter("@FECHAF", vFechaFin),
            };
            return Configuracion.EjecutarProcedure(_comando, pParametros, out except);

        }

        public static DataTable PSolicitud(out string except, int vOpcion, Int64 vIdSolicitud, string vSolicitud, string vEstatus, DateTime vFecha, int vEstadoSolicitud, string vIdPaquete, string vEstadoDescarga, int vAuto, string vTipo, string vBusqueda, string vTipoDescarga)
        {
            SqlCommand _comando = Configuracion.CrearComandoProc("");
            _comando.CommandText = "PSolicitud;1";
            SqlParameter[] pParametros = new SqlParameter[]{
                 new SqlParameter("@Opcion", vOpcion),
                 new SqlParameter("@IdSolicitud", vIdSolicitud),
                 new SqlParameter("@Solicitud", vSolicitud),
                 new SqlParameter("@Estatus", vEstatus),
                 new SqlParameter("@Fecha", vFecha),
                 new SqlParameter("@EstadoSolicitud", vEstadoSolicitud),
                 new SqlParameter("@IdPaquete", vIdPaquete),
                 new SqlParameter("@EstadoDescarga", vEstadoDescarga),
                 new SqlParameter("@Auto", vAuto),
                 new SqlParameter("@Tipo", vTipo),
                 new SqlParameter("@Busqueda", vBusqueda),
                 new SqlParameter("@TipoDescarga", vTipoDescarga),
            };
            return Configuracion.EjecutarProcedure(_comando, pParametros, out except);
        }

        public static DataTable PCargaDatos(out string except, string vTabla, string vRuta)
        {
            SqlCommand _comando = Configuracion.CrearComandoProc("");
            _comando.CommandText = "PCargaDatos;1";
            SqlParameter[] pParametros = new SqlParameter[]{
                 new SqlParameter("@tabla", vTabla),
                 new SqlParameter("@ruta", vRuta),
            };
            return Configuracion.EjecutarProcedure(_comando, pParametros, out except);

        }

        public static DataTable PMetaData(out string except, int vOpcion, string vRfcEmisor, string vNombreEmisor, string vRfcReceptor, string vNombreReceptor, string vRfcPac, string vFechaEmision,
        string vFechaCertificacionSat, string vMonto, string vEfectoComprobante, string vEstatus, string vFechaCancelacion, string vUUID)
        {
            SqlCommand _comando = Configuracion.CrearComandoProc("");
            _comando.CommandText = "PMetaData;1";
            SqlParameter[] pParametros = new SqlParameter[]{
                 new SqlParameter("@Uuid", vUUID),
                 new SqlParameter("@RfcEmisor", vRfcEmisor),
                 new SqlParameter("@NombreEmisor", vNombreEmisor),
                 new SqlParameter("@RfcReceptor", vRfcReceptor),
                 new SqlParameter("@NombreReceptor", vNombreReceptor),
                 new SqlParameter("@RfcPac", vRfcPac),
                 new SqlParameter("@FechaEmision", vFechaEmision),
                 new SqlParameter("@FechaCertificacionSat", vFechaCertificacionSat),
                 new SqlParameter("@Monto", vMonto),
                 new SqlParameter("@EfectoComprobante", vEfectoComprobante),
                 new SqlParameter("@Estatus", vEstatus),
                 new SqlParameter("@FechaCancelacion", vFechaCancelacion),
                 new SqlParameter("@Opcion", vOpcion),
            };
            return Configuracion.EjecutarProcedure(_comando, pParametros, out except);
        }


    }
}

