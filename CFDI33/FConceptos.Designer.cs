﻿namespace CFDI33
{
    partial class FConceptos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FConceptos));
            this.gCFDI = new DevExpress.XtraGrid.GridControl();
            this.gvCFDI = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gConceptos = new DevExpress.XtraGrid.GridControl();
            this.gvConceptos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.progress = new DevExpress.XtraWaitForm.ProgressPanel();
            this.btn_XML = new DevExpress.XtraEditors.SimpleButton();
            this.btn_PDF = new DevExpress.XtraEditors.SimpleButton();
            this.btn_Salir = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvConceptos)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gCFDI
            // 
            this.gCFDI.Dock = System.Windows.Forms.DockStyle.Top;
            this.gCFDI.Location = new System.Drawing.Point(0, 0);
            this.gCFDI.MainView = this.gvCFDI;
            this.gCFDI.Name = "gCFDI";
            this.gCFDI.Size = new System.Drawing.Size(1278, 148);
            this.gCFDI.TabIndex = 0;
            this.gCFDI.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCFDI});
            // 
            // gvCFDI
            // 
            this.gvCFDI.Appearance.HeaderPanel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvCFDI.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvCFDI.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvCFDI.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvCFDI.Appearance.Row.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvCFDI.Appearance.Row.Options.UseFont = true;
            this.gvCFDI.Appearance.Row.Options.UseTextOptions = true;
            this.gvCFDI.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvCFDI.Appearance.ViewCaption.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvCFDI.Appearance.ViewCaption.Options.UseFont = true;
            this.gvCFDI.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gvCFDI.GridControl = this.gCFDI;
            this.gvCFDI.Name = "gvCFDI";
            this.gvCFDI.OptionsBehavior.Editable = false;
            this.gvCFDI.OptionsView.ShowFooter = true;
            this.gvCFDI.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gvCFDI.OptionsView.ShowGroupPanel = false;
            this.gvCFDI.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvCFDI.OptionsView.ShowIndicator = false;
            this.gvCFDI.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvCFDI.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvCFDI.OptionsView.ShowViewCaption = true;
            this.gvCFDI.ViewCaption = "CFDI";
            // 
            // gConceptos
            // 
            this.gConceptos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gConceptos.Location = new System.Drawing.Point(0, 148);
            this.gConceptos.MainView = this.gvConceptos;
            this.gConceptos.Name = "gConceptos";
            this.gConceptos.Size = new System.Drawing.Size(1278, 446);
            this.gConceptos.TabIndex = 1;
            this.gConceptos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvConceptos});
            // 
            // gvConceptos
            // 
            this.gvConceptos.Appearance.HeaderPanel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvConceptos.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvConceptos.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvConceptos.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvConceptos.Appearance.Row.Options.UseTextOptions = true;
            this.gvConceptos.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvConceptos.Appearance.ViewCaption.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvConceptos.Appearance.ViewCaption.Options.UseFont = true;
            this.gvConceptos.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gvConceptos.GridControl = this.gConceptos;
            this.gvConceptos.Name = "gvConceptos";
            this.gvConceptos.OptionsBehavior.Editable = false;
            this.gvConceptos.OptionsView.ShowFooter = true;
            this.gvConceptos.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gvConceptos.OptionsView.ShowGroupPanel = false;
            this.gvConceptos.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvConceptos.OptionsView.ShowIndicator = false;
            this.gvConceptos.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvConceptos.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvConceptos.OptionsView.ShowViewCaption = true;
            this.gvConceptos.ViewCaption = "Conceptos";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.simpleButton2);
            this.panel1.Controls.Add(this.simpleButton1);
            this.panel1.Controls.Add(this.progress);
            this.panel1.Controls.Add(this.btn_XML);
            this.panel1.Controls.Add(this.btn_PDF);
            this.panel1.Controls.Add(this.btn_Salir);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 528);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1278, 66);
            this.panel1.TabIndex = 2;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(685, 12);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(124, 44);
            this.simpleButton2.TabIndex = 5;
            this.simpleButton2.Text = "P&DF";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(528, 10);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(124, 44);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "X&ML";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // progress
            // 
            this.progress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progress.Appearance.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progress.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.progress.Appearance.Options.UseBackColor = true;
            this.progress.Appearance.Options.UseFont = true;
            this.progress.Appearance.Options.UseForeColor = true;
            this.progress.AppearanceCaption.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progress.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.progress.AppearanceCaption.Options.UseFont = true;
            this.progress.AppearanceCaption.Options.UseForeColor = true;
            this.progress.BarAnimationElementThickness = 2;
            this.progress.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.progress.Caption = "Generando PDF";
            this.progress.Description = "Espere por Favor ...";
            this.progress.Location = new System.Drawing.Point(259, 3);
            this.progress.LookAndFeel.SkinName = "Liquid Sky";
            this.progress.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(308, 53);
            this.progress.TabIndex = 3;
            this.progress.Text = "Gerando PDF";
            this.progress.UseWaitCursor = true;
            this.progress.Visible = false;
            // 
            // btn_XML
            // 
            this.btn_XML.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_XML.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_XML.Appearance.Options.UseFont = true;
            this.btn_XML.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_XML.ImageOptions.Image")));
            this.btn_XML.Location = new System.Drawing.Point(838, 10);
            this.btn_XML.Name = "btn_XML";
            this.btn_XML.Size = new System.Drawing.Size(124, 44);
            this.btn_XML.TabIndex = 2;
            this.btn_XML.Text = "&XML";
            this.btn_XML.Click += new System.EventHandler(this.btn_XML_Click);
            // 
            // btn_PDF
            // 
            this.btn_PDF.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_PDF.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_PDF.Appearance.Options.UseFont = true;
            this.btn_PDF.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_PDF.ImageOptions.Image")));
            this.btn_PDF.Location = new System.Drawing.Point(986, 10);
            this.btn_PDF.Name = "btn_PDF";
            this.btn_PDF.Size = new System.Drawing.Size(124, 44);
            this.btn_PDF.TabIndex = 1;
            this.btn_PDF.Text = "&PDF";
            this.btn_PDF.Click += new System.EventHandler(this.btn_PDF_Click);
            // 
            // btn_Salir
            // 
            this.btn_Salir.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btn_Salir.Appearance.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Salir.Appearance.Options.UseFont = true;
            this.btn_Salir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btn_Salir.ImageOptions.Image")));
            this.btn_Salir.Location = new System.Drawing.Point(1133, 10);
            this.btn_Salir.Name = "btn_Salir";
            this.btn_Salir.Size = new System.Drawing.Size(124, 44);
            this.btn_Salir.TabIndex = 0;
            this.btn_Salir.Text = "&Salir";
            this.btn_Salir.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // FConceptos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1278, 594);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gConceptos);
            this.Controls.Add(this.gCFDI);
            this.Name = "FConceptos";
            this.Text = "Detalle de CFDI";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.FConceptos_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.gCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvConceptos)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gCFDI;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCFDI;
        private DevExpress.XtraGrid.GridControl gConceptos;
        private DevExpress.XtraGrid.Views.Grid.GridView gvConceptos;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btn_PDF;
        private DevExpress.XtraEditors.SimpleButton btn_Salir;
        private DevExpress.XtraEditors.SimpleButton btn_XML;
        private DevExpress.XtraWaitForm.ProgressPanel progress;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}