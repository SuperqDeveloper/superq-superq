﻿namespace CFDI33
{
    partial class FSAT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSAT));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pic_logo = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.button1 = new System.Windows.Forms.Button();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnExportar = new DevExpress.XtraEditors.SimpleButton();
            this.dateFin = new DevExpress.XtraEditors.DateEdit();
            this.dateInicio = new DevExpress.XtraEditors.DateEdit();
            this.labFFin = new DevExpress.XtraEditors.LabelControl();
            this.labFInicio = new DevExpress.XtraEditors.LabelControl();
            this.txtReceptor = new DevExpress.XtraEditors.TextEdit();
            this.labReceptor = new DevExpress.XtraEditors.LabelControl();
            this.txtEmisor = new DevExpress.XtraEditors.TextEdit();
            this.labEmisor = new DevExpress.XtraEditors.LabelControl();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.splashManage = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::CFDI33.WaitForm), true, true);
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.progress = new DevExpress.XtraWaitForm.ProgressPanel();
            this.tProceso = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceptor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmisor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.pic_logo);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1143, 74);
            this.panelControl1.TabIndex = 0;
            // 
            // pic_logo
            // 
            this.pic_logo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pic_logo.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pic_logo.EditValue = ((object)(resources.GetObject("pic_logo.EditValue")));
            this.pic_logo.Location = new System.Drawing.Point(840, -27);
            this.pic_logo.Name = "pic_logo";
            this.pic_logo.Properties.AllowFocused = false;
            this.pic_logo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pic_logo.Properties.Appearance.Options.UseBackColor = true;
            this.pic_logo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pic_logo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pic_logo.Properties.ZoomAccelerationFactor = 1D;
            this.pic_logo.Size = new System.Drawing.Size(291, 120);
            this.pic_logo.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Roboto", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Navy;
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(333, 38);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Descarga desde el SAT";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.button1);
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Controls.Add(this.btnExportar);
            this.panelControl2.Controls.Add(this.dateFin);
            this.panelControl2.Controls.Add(this.dateInicio);
            this.panelControl2.Controls.Add(this.labFFin);
            this.panelControl2.Controls.Add(this.labFInicio);
            this.panelControl2.Controls.Add(this.txtReceptor);
            this.panelControl2.Controls.Add(this.labReceptor);
            this.panelControl2.Controls.Add(this.txtEmisor);
            this.panelControl2.Controls.Add(this.labEmisor);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 74);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1143, 117);
            this.panelControl2.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1004, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Simula Proceso";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.simpleButton1.Location = new System.Drawing.Point(985, 26);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(146, 72);
            this.simpleButton1.TabIndex = 15;
            this.simpleButton1.Text = "&Verificar";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnExportar
            // 
            this.btnExportar.Appearance.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportar.Appearance.Options.UseFont = true;
            this.btnExportar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExportar.ImageOptions.Image")));
            this.btnExportar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnExportar.Location = new System.Drawing.Point(782, 26);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(180, 72);
            this.btnExportar.TabIndex = 14;
            this.btnExportar.Text = "&Descargar";
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // dateFin
            // 
            this.dateFin.EditValue = null;
            this.dateFin.Location = new System.Drawing.Point(517, 74);
            this.dateFin.Name = "dateFin";
            this.dateFin.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateFin.Properties.Appearance.Options.UseFont = true;
            this.dateFin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFin.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFin.Size = new System.Drawing.Size(237, 26);
            this.dateFin.TabIndex = 8;
            // 
            // dateInicio
            // 
            this.dateInicio.EditValue = null;
            this.dateInicio.Location = new System.Drawing.Point(126, 74);
            this.dateInicio.Name = "dateInicio";
            this.dateInicio.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateInicio.Properties.Appearance.Options.UseFont = true;
            this.dateInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateInicio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateInicio.Size = new System.Drawing.Size(233, 26);
            this.dateInicio.TabIndex = 7;
            // 
            // labFFin
            // 
            this.labFFin.Appearance.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFFin.Appearance.Options.UseFont = true;
            this.labFFin.Location = new System.Drawing.Point(397, 76);
            this.labFFin.Name = "labFFin";
            this.labFFin.Size = new System.Drawing.Size(69, 19);
            this.labFFin.TabIndex = 6;
            this.labFFin.Text = "Fecha &Fin";
            // 
            // labFInicio
            // 
            this.labFInicio.Appearance.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFInicio.Appearance.Options.UseFont = true;
            this.labFInicio.Location = new System.Drawing.Point(27, 77);
            this.labFInicio.Name = "labFInicio";
            this.labFInicio.Size = new System.Drawing.Size(86, 19);
            this.labFInicio.TabIndex = 4;
            this.labFInicio.Text = "Fecha &Inicio";
            // 
            // txtReceptor
            // 
            this.txtReceptor.EditValue = "XAXX010101000";
            this.txtReceptor.Location = new System.Drawing.Point(517, 23);
            this.txtReceptor.Name = "txtReceptor";
            this.txtReceptor.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceptor.Properties.Appearance.Options.UseFont = true;
            this.txtReceptor.Size = new System.Drawing.Size(237, 26);
            this.txtReceptor.TabIndex = 3;
            // 
            // labReceptor
            // 
            this.labReceptor.Appearance.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labReceptor.Appearance.Options.UseFont = true;
            this.labReceptor.Location = new System.Drawing.Point(397, 29);
            this.labReceptor.Name = "labReceptor";
            this.labReceptor.Size = new System.Drawing.Size(98, 19);
            this.labReceptor.TabIndex = 2;
            this.labReceptor.Text = "RFC &Receptor";
            // 
            // txtEmisor
            // 
            this.txtEmisor.EditValue = "SQX981027RY5";
            this.txtEmisor.Location = new System.Drawing.Point(126, 23);
            this.txtEmisor.Name = "txtEmisor";
            this.txtEmisor.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmisor.Properties.Appearance.Options.UseFont = true;
            this.txtEmisor.Size = new System.Drawing.Size(233, 26);
            this.txtEmisor.TabIndex = 1;
            // 
            // labEmisor
            // 
            this.labEmisor.Appearance.Font = new System.Drawing.Font("Roboto", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEmisor.Appearance.Options.UseFont = true;
            this.labEmisor.Location = new System.Drawing.Point(27, 29);
            this.labEmisor.Name = "labEmisor";
            this.labEmisor.Size = new System.Drawing.Size(84, 19);
            this.labEmisor.TabIndex = 0;
            this.labEmisor.Text = "RFC &Emisor";
            // 
            // txtLog
            // 
            this.txtLog.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.txtLog.CausesValidation = false;
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtLog.Location = new System.Drawing.Point(0, 191);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(1143, 379);
            this.txtLog.TabIndex = 2;
            this.txtLog.WordWrap = false;
            this.txtLog.TextChanged += new System.EventHandler(this.txtLog_TextChanged);
            // 
            // splashManage
            // 
            this.splashManage.ClosingDelay = 500;
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.progress);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl3.Location = new System.Drawing.Point(0, 494);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(1143, 76);
            this.panelControl3.TabIndex = 3;
            // 
            // progress
            // 
            this.progress.AnimationAcceleration = 10F;
            this.progress.AnimationElementCount = 10;
            this.progress.AnimationSpeed = 5F;
            this.progress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progress.Appearance.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progress.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.progress.Appearance.Options.UseBackColor = true;
            this.progress.Appearance.Options.UseFont = true;
            this.progress.Appearance.Options.UseForeColor = true;
            this.progress.AppearanceCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progress.AppearanceCaption.Options.UseFont = true;
            this.progress.AppearanceDescription.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progress.AppearanceDescription.ForeColor = System.Drawing.Color.Navy;
            this.progress.AppearanceDescription.Options.UseFont = true;
            this.progress.AppearanceDescription.Options.UseForeColor = true;
            this.progress.BarAnimationElementThickness = 2;
            this.progress.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.progress.ContentAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.progress.Description = "Procesando ...";
            this.progress.Dock = System.Windows.Forms.DockStyle.Left;
            this.progress.ImageHorzOffset = 20;
            this.progress.Location = new System.Drawing.Point(2, 2);
            this.progress.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.progress.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progress.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(310, 72);
            this.progress.TabIndex = 1;
            this.progress.Text = "progressPanel1";
            // 
            // tProceso
            // 
            this.tProceso.Enabled = true;
            this.tProceso.Interval = 300000;
            this.tProceso.Tick += new System.EventHandler(this.tProceso_Tick);
            // 
            // FSAT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 570);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Name = "FSAT";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FSAT";
            this.Load += new System.EventHandler(this.FSAT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReceptor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmisor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.PictureEdit pic_logo;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit txtEmisor;
        private DevExpress.XtraEditors.LabelControl labEmisor;
        private DevExpress.XtraEditors.TextEdit txtReceptor;
        private DevExpress.XtraEditors.LabelControl labReceptor;
        private DevExpress.XtraEditors.DateEdit dateFin;
        private DevExpress.XtraEditors.DateEdit dateInicio;
        private DevExpress.XtraEditors.LabelControl labFFin;
        private DevExpress.XtraEditors.LabelControl labFInicio;
        private DevExpress.XtraEditors.SimpleButton btnExportar;
        private System.Windows.Forms.TextBox txtLog;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashManage;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraWaitForm.ProgressPanel progress;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Timer tProceso;
        private System.Windows.Forms.Button button1;
    }
}