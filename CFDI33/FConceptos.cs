﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace CFDI33
{
    public partial class FConceptos : DevExpress.XtraEditors.XtraForm
    {
        public Int64 vCFDI;
        public FConceptos()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool CargaDatos(string vRFC, string vFechaIni, string vFechaFin, Int64 vCFDI)
        {
            string except = "";
            try
            {
                gCFDI.DataSource = Negocio.CNegocio.BuscaCDFI(out except, 3, "", "", "", "", "", "", "", "", "", "", 0, 0, "", 0, "", "", "", "", "", vRFC, vFechaIni, vFechaFin, vCFDI, "");
                CheckMinimumWidth(gvCFDI);
                if (gvCFDI.RowCount < 1)
                    return false;
                else
                    return true;
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                MessageBox.Show("Error: Al Buscar CFDI'S" + except + " " + Ex.Message, "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return false;
            }
        }




        private void FConceptos_Shown(object sender, EventArgs e)
        {
            progress.Visible = false;
            CargaDatos("", "", "", vCFDI);
            CargaConceptos(vCFDI);
        }

        private void CheckMinimumWidth(DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            GridViewInfo viewInfo = (GridViewInfo)view.GetViewInfo();
            if (viewInfo.ColumnsInfo.LastColumnInfo.Bounds.Right > viewInfo.ViewRects.ColumnPanel.Right)
            {
                view.OptionsView.ColumnAutoWidth = false;
                view.BestFitColumns();
            }
            else view.OptionsView.ColumnAutoWidth = true;
        }

        private void CargaConceptos(Int64 vCFDI)
        {
            string except="";
            try
            {
                gConceptos.DataSource = Negocio.CNegocio.BuscaConceptos(out except, 1, 0, vCFDI, "", "", 0, "", "", 0, 0);
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                MessageBox.Show("Error: Al Buscar Conceptos CFDI'S" + except + " " + Ex.Message, "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }

        private void btn_PDF_Click(object sender, EventArgs e)
        {
            string vUUID="";
            try
            {
                //Negocio.CNegocio.generaPDf(gvCFDI.GetRowCellValue(gvCFDI.FocusedRowHandle, "Archivo").ToString());
                progress.Visible = true;
                Application.DoEvents();
                Negocio.CNegocio.GeneraPDf(Negocio.CNegocio.GetXml(Convert.ToInt64(gvCFDI.GetRowCellValue(gvCFDI.FocusedRowHandle, "IDCFDI").ToString()), out vUUID),  vUUID);
                Application.DoEvents();
                progress.Visible = false;
            }
            catch(Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                MessageBox.Show(Ex.Message, "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                progress.Visible = false;
            }
        }

        private void btn_XML_Click(object sender, EventArgs e)
        {
            /*string verror = "";
            try
            {
                if (!Negocio.CNegocio.AbrirArchivo(gvCFDI.GetRowCellValue(gvCFDI.FocusedRowHandle, "Archivo").ToString(), out verror))
                    MessageBox.Show(verror, "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
            }
            */
            string except = "";
            string vXml = "";
            string vUUID = "";
            try
            {
                FXml fXml = new FXml();
                vXml = Negocio.CNegocio.GetXml(Convert.ToInt64(gvCFDI.GetRowCellValue(gvCFDI.FocusedRowHandle, "IDCFDI").ToString()), out vUUID) ;
                //fXml.vIDCFDI = vUUID;
                fXml.wbXML.DocumentText = vXml;
                //fXml.tx
                fXml.ShowDialog();
            }
            catch(Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                MessageBox.Show("Error al abrir XML "+Ex.Message, "SUPERQ CFDI's", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }


        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            try
            {
                Negocio.CNegocio.Creacarpeta(@Application.StartupPath.ToString() + "\\XML");
                System.Diagnostics.Process.Start(@Application.StartupPath.ToString() + "\\XML");
            }
            catch
            {
                MessageBox.Show(new Form() { TopMost = true }, "No existe la carpeta ", "ENVIO SAP", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                Negocio.CNegocio.Creacarpeta(@Application.StartupPath.ToString() + "\\PDF");
                System.Diagnostics.Process.Start(@Application.StartupPath.ToString() + "\\PDF");
            }
            catch
            {
                MessageBox.Show(new Form() { TopMost = true }, "No existe la carpeta ", "ENVIO SAP", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }
    }
}