﻿namespace CFDI33
{
    partial class CFDI33
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CFDI33));
            this.pic_Cintillo = new DevExpress.XtraEditors.PictureEdit();
            this.pic_logo = new DevExpress.XtraEditors.PictureEdit();
            this.txtFolio = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnImportar = new DevExpress.XtraEditors.SimpleButton();
            this.btnExportar = new DevExpress.XtraEditors.SimpleButton();
            this.btnBuscar = new DevExpress.XtraEditors.SimpleButton();
            this.dateFin = new DevExpress.XtraEditors.DateEdit();
            this.dateInicio = new DevExpress.XtraEditors.DateEdit();
            this.laFechas = new System.Windows.Forms.Label();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.txtSerie = new DevExpress.XtraEditors.TextEdit();
            this.labFolio = new System.Windows.Forms.Label();
            this.labSerie = new System.Windows.Forms.Label();
            this.txtNomEmi = new DevExpress.XtraEditors.TextEdit();
            this.txtRfcEmi = new DevExpress.XtraEditors.TextEdit();
            this.labNomEmi = new System.Windows.Forms.Label();
            this.labRfcEmi = new System.Windows.Forms.Label();
            this.status = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.file = new System.Windows.Forms.OpenFileDialog();
            this.saveFile = new System.Windows.Forms.SaveFileDialog();
            this.gCFDI = new DevExpress.XtraGrid.GridControl();
            this.gvCFDI = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.splashManage = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::CFDI33.WaitForm), true, true);
            this.tAuto = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Cintillo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFolio)).BeginInit();
            this.txtFolio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerie.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomEmi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRfcEmi.Properties)).BeginInit();
            this.status.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gCFDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCFDI)).BeginInit();
            this.SuspendLayout();
            // 
            // pic_Cintillo
            // 
            this.pic_Cintillo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pic_Cintillo.Cursor = System.Windows.Forms.Cursors.Default;
            this.pic_Cintillo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pic_Cintillo.EditValue = ((object)(resources.GetObject("pic_Cintillo.EditValue")));
            this.pic_Cintillo.Location = new System.Drawing.Point(0, 0);
            this.pic_Cintillo.Margin = new System.Windows.Forms.Padding(0);
            this.pic_Cintillo.Name = "pic_Cintillo";
            this.pic_Cintillo.Properties.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.pic_Cintillo.Properties.AllowFocused = false;
            this.pic_Cintillo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pic_Cintillo.Properties.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.pic_Cintillo.Properties.Appearance.Options.UseBackColor = true;
            this.pic_Cintillo.Properties.Appearance.Options.UseForeColor = true;
            this.pic_Cintillo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pic_Cintillo.Properties.ShowMenu = false;
            this.pic_Cintillo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pic_Cintillo.Properties.ZoomAccelerationFactor = 1D;
            this.pic_Cintillo.Size = new System.Drawing.Size(1290, 72);
            this.pic_Cintillo.TabIndex = 3;
            // 
            // pic_logo
            // 
            this.pic_logo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pic_logo.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pic_logo.EditValue = ((object)(resources.GetObject("pic_logo.EditValue")));
            this.pic_logo.Location = new System.Drawing.Point(1170, 12);
            this.pic_logo.Name = "pic_logo";
            this.pic_logo.Properties.AllowFocused = false;
            this.pic_logo.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pic_logo.Properties.Appearance.Options.UseBackColor = true;
            this.pic_logo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pic_logo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pic_logo.Properties.ZoomAccelerationFactor = 1D;
            this.pic_logo.Size = new System.Drawing.Size(120, 60);
            this.pic_logo.TabIndex = 4;
            // 
            // txtFolio
            // 
            this.txtFolio.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtFolio.Controls.Add(this.simpleButton2);
            this.txtFolio.Controls.Add(this.simpleButton1);
            this.txtFolio.Controls.Add(this.btnImportar);
            this.txtFolio.Controls.Add(this.btnExportar);
            this.txtFolio.Controls.Add(this.btnBuscar);
            this.txtFolio.Controls.Add(this.dateFin);
            this.txtFolio.Controls.Add(this.dateInicio);
            this.txtFolio.Controls.Add(this.laFechas);
            this.txtFolio.Controls.Add(this.textEdit2);
            this.txtFolio.Controls.Add(this.txtSerie);
            this.txtFolio.Controls.Add(this.labFolio);
            this.txtFolio.Controls.Add(this.labSerie);
            this.txtFolio.Controls.Add(this.txtNomEmi);
            this.txtFolio.Controls.Add(this.txtRfcEmi);
            this.txtFolio.Controls.Add(this.labNomEmi);
            this.txtFolio.Controls.Add(this.labRfcEmi);
            this.txtFolio.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFolio.Location = new System.Drawing.Point(0, 72);
            this.txtFolio.Name = "txtFolio";
            this.txtFolio.Size = new System.Drawing.Size(1290, 128);
            this.txtFolio.TabIndex = 5;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.simpleButton2.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(1099, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(54, 51);
            this.simpleButton2.TabIndex = 16;
            this.simpleButton2.Text = "&Meta";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.simpleButton1.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(1159, 69);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(109, 51);
            this.simpleButton1.TabIndex = 15;
            this.simpleButton1.Text = "&SAT";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnImportar
            // 
            this.btnImportar.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnImportar.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportar.Appearance.Options.UseFont = true;
            this.btnImportar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnImportar.ImageOptions.Image")));
            this.btnImportar.Location = new System.Drawing.Point(1159, 5);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(109, 51);
            this.btnImportar.TabIndex = 14;
            this.btnImportar.Text = "&Importar";
            this.btnImportar.Click += new System.EventHandler(this.btnImportar_Click);
            // 
            // btnExportar
            // 
            this.btnExportar.Appearance.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportar.Appearance.Options.UseFont = true;
            this.btnExportar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExportar.ImageOptions.Image")));
            this.btnExportar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnExportar.Location = new System.Drawing.Point(893, 22);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(180, 72);
            this.btnExportar.TabIndex = 13;
            this.btnExportar.Text = "&Exportar";
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Appearance.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Appearance.Options.UseFont = true;
            this.btnBuscar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.ImageOptions.Image")));
            this.btnBuscar.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.btnBuscar.Location = new System.Drawing.Point(683, 22);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(180, 74);
            this.btnBuscar.TabIndex = 11;
            this.btnBuscar.Text = "&Buscar";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // dateFin
            // 
            this.dateFin.EditValue = null;
            this.dateFin.Location = new System.Drawing.Point(415, 91);
            this.dateFin.Name = "dateFin";
            this.dateFin.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateFin.Properties.Appearance.Options.UseFont = true;
            this.dateFin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFin.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateFin.Size = new System.Drawing.Size(235, 26);
            this.dateFin.TabIndex = 10;
            // 
            // dateInicio
            // 
            this.dateInicio.EditValue = null;
            this.dateInicio.Location = new System.Drawing.Point(142, 91);
            this.dateInicio.Name = "dateInicio";
            this.dateInicio.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateInicio.Properties.Appearance.Options.UseFont = true;
            this.dateInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateInicio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateInicio.Size = new System.Drawing.Size(248, 26);
            this.dateInicio.TabIndex = 9;
            // 
            // laFechas
            // 
            this.laFechas.AutoSize = true;
            this.laFechas.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laFechas.Location = new System.Drawing.Point(82, 90);
            this.laFechas.Name = "laFechas";
            this.laFechas.Size = new System.Drawing.Size(54, 19);
            this.laFechas.TabIndex = 8;
            this.laFechas.Text = "Fechas";
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(554, 49);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Size = new System.Drawing.Size(96, 26);
            this.textEdit2.TabIndex = 7;
            // 
            // txtSerie
            // 
            this.txtSerie.Location = new System.Drawing.Point(554, 11);
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerie.Properties.Appearance.Options.UseFont = true;
            this.txtSerie.Size = new System.Drawing.Size(96, 26);
            this.txtSerie.TabIndex = 6;
            // 
            // labFolio
            // 
            this.labFolio.AutoSize = true;
            this.labFolio.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labFolio.Location = new System.Drawing.Point(505, 52);
            this.labFolio.Name = "labFolio";
            this.labFolio.Size = new System.Drawing.Size(42, 19);
            this.labFolio.TabIndex = 5;
            this.labFolio.Text = "Folio";
            // 
            // labSerie
            // 
            this.labSerie.AutoSize = true;
            this.labSerie.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSerie.Location = new System.Drawing.Point(505, 18);
            this.labSerie.Name = "labSerie";
            this.labSerie.Size = new System.Drawing.Size(43, 19);
            this.labSerie.TabIndex = 4;
            this.labSerie.Text = "Serie";
            // 
            // txtNomEmi
            // 
            this.txtNomEmi.Location = new System.Drawing.Point(142, 45);
            this.txtNomEmi.Name = "txtNomEmi";
            this.txtNomEmi.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomEmi.Properties.Appearance.Options.UseFont = true;
            this.txtNomEmi.Size = new System.Drawing.Size(326, 26);
            this.txtNomEmi.TabIndex = 3;
            // 
            // txtRfcEmi
            // 
            this.txtRfcEmi.Location = new System.Drawing.Point(142, 11);
            this.txtRfcEmi.Name = "txtRfcEmi";
            this.txtRfcEmi.Properties.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRfcEmi.Properties.Appearance.Options.UseFont = true;
            this.txtRfcEmi.Size = new System.Drawing.Size(326, 26);
            this.txtRfcEmi.TabIndex = 2;
            // 
            // labNomEmi
            // 
            this.labNomEmi.AutoSize = true;
            this.labNomEmi.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNomEmi.Location = new System.Drawing.Point(21, 52);
            this.labNomEmi.Name = "labNomEmi";
            this.labNomEmi.Size = new System.Drawing.Size(115, 19);
            this.labNomEmi.TabIndex = 1;
            this.labNomEmi.Text = "Nombre Emisor";
            // 
            // labRfcEmi
            // 
            this.labRfcEmi.AutoSize = true;
            this.labRfcEmi.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labRfcEmi.Location = new System.Drawing.Point(53, 18);
            this.labRfcEmi.Name = "labRfcEmi";
            this.labRfcEmi.Size = new System.Drawing.Size(83, 19);
            this.labRfcEmi.TabIndex = 0;
            this.labRfcEmi.Text = "RFC Emisor";
            // 
            // status
            // 
            this.status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.status.Location = new System.Drawing.Point(0, 635);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(1290, 22);
            this.status.TabIndex = 7;
            this.status.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // file
            // 
            this.file.AddExtension = false;
            this.file.DefaultExt = "*.xlsx";
            this.file.Filter = "Excel|*.xlsx";
            // 
            // saveFile
            // 
            this.saveFile.DefaultExt = "*.xlsx";
            this.saveFile.Filter = "Excel|*.xlsx";
            // 
            // gCFDI
            // 
            this.gCFDI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gCFDI.Location = new System.Drawing.Point(0, 200);
            this.gCFDI.MainView = this.gvCFDI;
            this.gCFDI.Name = "gCFDI";
            this.gCFDI.Size = new System.Drawing.Size(1290, 435);
            this.gCFDI.TabIndex = 8;
            this.gCFDI.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCFDI});
            this.gCFDI.DoubleClick += new System.EventHandler(this.gCFDI_DoubleClick_1);
            // 
            // gvCFDI
            // 
            this.gvCFDI.Appearance.HeaderPanel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvCFDI.Appearance.HeaderPanel.Options.UseFont = true;
            this.gvCFDI.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gvCFDI.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvCFDI.Appearance.Row.Options.UseTextOptions = true;
            this.gvCFDI.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvCFDI.GridControl = this.gCFDI;
            this.gvCFDI.Name = "gvCFDI";
            this.gvCFDI.OptionsBehavior.Editable = false;
            // 
            // splashManage
            // 
            this.splashManage.ClosingDelay = 500;
            // 
            // tAuto
            // 
            this.tAuto.Enabled = true;
            this.tAuto.Interval = 10000;
            this.tAuto.Tick += new System.EventHandler(this.tAuto_Tick);
            // 
            // CFDI33
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1290, 657);
            this.Controls.Add(this.gCFDI);
            this.Controls.Add(this.status);
            this.Controls.Add(this.txtFolio);
            this.Controls.Add(this.pic_logo);
            this.Controls.Add(this.pic_Cintillo);
            this.LookAndFeel.SkinName = "Foggy";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "CFDI33";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "+";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.CFDI33_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Cintillo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFolio)).EndInit();
            this.txtFolio.ResumeLayout(false);
            this.txtFolio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateFin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateInicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerie.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomEmi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRfcEmi.Properties)).EndInit();
            this.status.ResumeLayout(false);
            this.status.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gCFDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCFDI)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pic_Cintillo;
        public DevExpress.XtraEditors.PictureEdit pic_logo;
        private DevExpress.XtraEditors.PanelControl txtFolio;
        private System.Windows.Forms.StatusStrip status;
        private DevExpress.XtraEditors.TextEdit txtNomEmi;
        private DevExpress.XtraEditors.TextEdit txtRfcEmi;
        private System.Windows.Forms.Label labNomEmi;
        private System.Windows.Forms.Label labRfcEmi;
        private System.Windows.Forms.Label labSerie;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit txtSerie;
        private System.Windows.Forms.Label labFolio;
        private DevExpress.XtraEditors.DateEdit dateFin;
        private DevExpress.XtraEditors.DateEdit dateInicio;
        private System.Windows.Forms.Label laFechas;
        private DevExpress.XtraEditors.SimpleButton btnBuscar;
        private DevExpress.XtraEditors.SimpleButton btnImportar;
        private DevExpress.XtraEditors.SimpleButton btnExportar;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.OpenFileDialog file;
        private System.Windows.Forms.SaveFileDialog saveFile;
        private DevExpress.XtraGrid.GridControl gCFDI;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCFDI;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashManage;
        private System.Windows.Forms.Timer tAuto;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}

