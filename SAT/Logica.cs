﻿using System;
using System.IO;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace SAT
{
    public class Logica
    {
        static byte[] pfx = File.ReadAllBytes(@"Resources\pfx.pfx");
        static string password = "12345";

        static string urlAutentica = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/Autenticacion/Autenticacion.svc";
        static string urlAutenticaAction = "http://DescargaMasivaTerceros.gob.mx/IAutenticacion/Autentica";

        static string urlSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/SolicitaDescargaService.svc";
        static string urlSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/ISolicitaDescargaService/SolicitaDescarga";

        static string urlVerificarSolicitud = "https://cfdidescargamasivasolicitud.clouda.sat.gob.mx/VerificaSolicitudDescargaService.svc";
        static string urlVerificarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IVerificaSolicitudDescargaService/VerificaSolicitudDescarga";

        static string urlDescargarSolicitud = "https://cfdidescargamasiva.clouda.sat.gob.mx/DescargaMasivaService.svc";
        static string urlDescargarSolicitudAction = "http://DescargaMasivaTerceros.sat.gob.mx/IDescargaMasivaTercerosService/Descargar";

        //static string RfcEmisor = "SQX981027RY5";
        //static string RfcReceptor = "SEGI820403RIA";
        static string RfcSolicita = "SQX981027RY5";
        //static string RfcSolicita = "SEGI820403RIA";
        //static string FechaInicial = "2018-01-01";
        //static string FechaFinal = "2019-08-01";

        public static X509Certificate2 certificate = ObtenerX509Certificado(pfx);

        static string token; // GetToken(certifcate);
        static string autorization; //= String.Format("WRAP access_token=\"{0}\"", HttpUtility.UrlDecode(token));

        public static void EscribeLog(string vLinea, string ruta)
        {
            try
            {
                string file;
                file = ruta + "\\Log\\" + Getfecha() + ".log";
                Creacarpeta(ruta + "\\Log");
                using (StreamWriter write = new StreamWriter(file, true))
                {
                    write.WriteLine(vLinea + ";" + Gethorafecha() + ";");
                    write.Close();
                }
            }
            catch (Exception Ex)
            {

                EscribeLog(Ex.Message, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                //MessageBox.Show("Error: Al Buscar CFDI'S" + " " + Ex.Message, "SUPERQ", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }

        public static string Getfecha()
        {
            return (DateTime.Now.ToString("ddMMyyyy"));
        }

        public static string Gethorafecha()
        {
            return DateTime.Now.ToString("ddMMyyyy HH:mm:ss");
        }

        public static void Creacarpeta(string ruta)
        {
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
        }

        public static X509Certificate2 ObtenerX509Certificado(byte[] pfx)
        {
            return new X509Certificate2(pfx, password, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
        }

        public static string GetToken(X509Certificate2 certifcate, out bool VResult)
        {
            Autenticacion service = new Autenticacion(urlAutentica, urlAutenticaAction);
            string xml = service.Generate(certifcate);
            return service.Send(out VResult);
        }

        public static string GetAutorization(string token)
        {
            return String.Format("WRAP access_token=\"{0}\"", HttpUtility.UrlDecode(token));
            
        }

        public static string GetSolicitud(string autorization, string RFCEmisor, string RFCReceptor, string Finicio, string FFin, out bool VResult, out string vCodEstatus, out string vMensaje, string vTipoDescarga)
        {
            //X509Certificate2 certificate;
            string vResultado= null;
            Solicitud solicitud = new Solicitud(urlSolicitud, urlSolicitudAction);
            string xmlSolicitud = solicitud.Generate(certificate, RFCEmisor, RFCReceptor, RfcSolicita, Finicio, FFin, vTipoDescarga);
            //EscribeLog(xmlSolicitud, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            vResultado =  solicitud.Send(out VResult, autorization);
            vCodEstatus = solicitud.vCodEstatus;
            vMensaje = solicitud.vMensaje;
            return vResultado;
        }

        public static string ValidaSolicitud(string autorization, string idSolicitud, string RfcReceptor, out bool VResult, out string vNumeroCFDI, out string vCodEstatus, out string vEstadoSol, out string vMensaje )
        {
            string vResultado=null;
            VerificaSolicitud verifica = new VerificaSolicitud(urlVerificarSolicitud, urlVerificarSolicitudAction);
            string xmlVerifica = verifica.Generate(certificate, RfcSolicita, idSolicitud);
            vResultado =verifica.Send(out VResult, autorization);
            vNumeroCFDI = verifica.vNumeroCFDI;
            vCodEstatus = verifica.vCodEstatus;
            vEstadoSol = verifica.vEstadoSolicitud;
            vMensaje = verifica.vMensaje;
            return vResultado;

        }

        public static string DescargaSolicitud(string autorization, string idPaquete, string RFCReceptor, out bool VResult)
        {
            DescargarSolicitud descargarSolicitud = new DescargarSolicitud(urlDescargarSolicitud, urlDescargarSolicitudAction);
            string xmlDescarga = descargarSolicitud.Generate(certificate, RfcSolicita, idPaquete);
            return descargarSolicitud.Send(out VResult, autorization);
        }

        public static bool DescargaArchivos(string idPaquete, string descargaResponse)
        {
            try
            {
                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Descargados\\";
                Creacarpeta(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Descargados\\");
                byte[] file = Convert.FromBase64String(descargaResponse);
                using (FileStream fs = File.Create(path + idPaquete + ".zip", file.Length))
                {
                    fs.Write(file, 0, file.Length);
                }
                return true;
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                return false;
            }
        }
    }

}
