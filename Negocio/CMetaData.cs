﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class CMetaData
    {
        private string vRfcEmisor; string vNombreEmisor; string vRfcReceptor; string vNombreReceptor; string vRfcPac; string vFechaEmision;
        string vFechaCertificacionSat; string vMonto; string vEfectoComprobante; string vEstatus; string vFechaCancelacion; string vUUID;

        public string VUUID { get => vUUID; set => vUUID = value; }
        public string VNombreEmisor { get => vNombreEmisor; set => vNombreEmisor = value; }
        public string VRfcReceptor { get => vRfcReceptor; set => vRfcReceptor = value; }
        public string VNombreReceptor { get => vNombreReceptor; set => vNombreReceptor = value; }
        public string VRfcPac { get => vRfcPac; set => vRfcPac = value; }
        public string VFechaEmision { get => vFechaEmision; set => vFechaEmision = value; }
        public string VFechaCertificacionSat { get => vFechaCertificacionSat; set => vFechaCertificacionSat = value; }
        public string VMonto { get => vMonto; set => vMonto = value; }
        public string VEfectoComprobante { get => vEfectoComprobante; set => vEfectoComprobante = value; }
        public string VEstatus { get => vEstatus; set => vEstatus = value; }
        public string VFechaCancelacion { get => vFechaCancelacion; set => vFechaCancelacion = value; }
        public string VRfcEmisor { get => vRfcEmisor; set => vRfcEmisor = value; }
    }
}
