﻿using DevExpress.XtraWaitForm;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using DevExpress.XtraSplashScreen;
using System.IO.Compression;
using System.Configuration;

namespace Negocio
{
    public class CNegocio
    {
        // crear log

        public static bool vProcesando;

        public static void EscribeLog(string vLinea)
        {
            try
            {
                string file;
                file = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Log\\" + Getfecha() + ".log";
                Creacarpeta(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Log");
                using (StreamWriter write = new StreamWriter(file, true))
                {
                    write.WriteLine(vLinea + ";" + Gethorafecha() + ";");
                    write.Close();
                }
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message);
                MessageBox.Show("Error: Al Buscar CFDI'S" + " " + Ex.Message, "SUPERQ", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }

        public static string Getfecha()
        {
            return (DateTime.Now.ToString("ddMMyyyy"));
        }

        public static string Gethorafecha()
        {
            return DateTime.Now.ToString("ddMMyyyy HH:mm:ss");
        }

        public static void Creacarpeta(string ruta)
        {
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
        }

        public static string GetVersion()
        {
            return Application.ProductVersion;
        }

        // Lee valores desde Appconfig
        public static string leerValorConfiguracion(string clave)
        {
            try
            {
                string resultado = ConfigurationManager.AppSettings[clave].ToString();
                return resultado;
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message);
                return "";
            }
        }

        public static string GetEntradConfig(string Key)
        {
            string s = ConfigurationManager.AppSettings[Key];
            if (!string.IsNullOrEmpty(s))
            {
                return s;
            }
            else
            {
                return "-1";
            }
        }

        public static bool RecoreCarpeta(string vRuta, WaitForm panel)
        {
            try
            {
                string[] listaFicheros = Directory.GetFiles(@Application.StartupPath + vRuta, "*.xml");
                foreach (string fichero in listaFicheros)
                    ProcesaArchivo(fichero, panel);
                return true;
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message);
                MessageBox.Show("Error: Al Buscar CFDI'S" + " " + Ex.Message, "SUPERQ", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return false;
            }
        }

        public static bool RecoreCarpetaMeta(string vRuta, WaitForm panel)
        {
            
            try
            {
                string[] listaFicheros = Directory.GetFiles(@Application.StartupPath + vRuta, "*.txt");
                foreach (string fichero in listaFicheros)
                    ProcesaArchivoMetaData(fichero, panel);
                return true;
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message);
                MessageBox.Show("Error: Al Buscar CFDI'S" + " " + Ex.Message, "SUPERQ", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                return false;
            }
        }

        public static void ProcesaArchivoMetaData(string fileName, WaitForm panel)
        {
            string except = "";
            CMetaData cMeta = new CMetaData();
            string file = fileName;
            try
            {
                Application.DoEvents();
                SplashScreenManager.Default.SetWaitFormDescription("Leyendo Archivo + "+fileName);
                Application.DoEvents();
                EscribeLog("Leyendo Archivo: "+file);
                using (StreamReader lector = new StreamReader(file))
                {
                    while (lector.Peek() > -1)
                    {
                        string linea = lector.ReadLine();
                        if (!String.IsNullOrEmpty(linea))
                        {
                            string[] valores = linea.Split('~');
                            if (valores[0] != "Uuid")
                            {
                                cMeta.VUUID = valores[0];
                                cMeta.VRfcEmisor = valores[1];
                                cMeta.VNombreEmisor = valores[2];
                                cMeta.VRfcReceptor = valores[3];
                                cMeta.VNombreReceptor = valores[4];
                                cMeta.VRfcPac = valores[5];
                                cMeta.VFechaEmision = valores[6];
                                cMeta.VFechaCertificacionSat = valores[7];
                                cMeta.VMonto = valores[8];
                                cMeta.VEfectoComprobante = valores[9];
                                cMeta.VEstatus = valores[10];
                                cMeta.VFechaCancelacion = valores[11];
                            }
                            PMetaData(out except, 2, cMeta.VRfcEmisor, cMeta.VNombreEmisor, cMeta.VRfcReceptor, cMeta.VNombreReceptor, cMeta.VRfcPac, cMeta.VFechaEmision, cMeta.VFechaCertificacionSat, cMeta.VMonto, cMeta.VEfectoComprobante, cMeta.VEstatus, cMeta.VFechaCancelacion, cMeta.VUUID);
                        }

                    }
                }
                 MoverArchivo(fileName, "\\CFDI\\Procesados\\METADATA");
            }
            catch(Exception Ex)
            {
                EscribeLog(Ex.Message);
            }            
        }

        public static void ProcesaArchivo(string fileName, WaitForm panel)
        {
            string vRFCEmisor; string vNombreEmisor; string vRegimenFiscal; string vRFCReceptor; string vNombreReceptor;
            string vUsoCFDI; string vSerie; string vFolio; string vFecha; string vFormaPago; double vSubtotal; double vTotal;
            string vMoneda; double vTipoCambio; string vTipoDeComprobante; string vMetodoPago; string vLugarExpedicion;
            string vUUID; string vFechaTimbrado; string vRFC; string vFechaIni; string vFechaFin; Int64 vIDCFDI; Int64 vIDConcepto; Int64 vIDImpuesto;
            string vClaveProdServ; string vNoIdentificacion; double vCantidad; string vClaveUnidad; string vDescripcion;
            double vValorUnitario; double vImporte; double vTrasladoBase; string vTrasladoImpuesto; string vTrasladoTipoFactor;
            double vTrasladoTasaOCuota; double vTrasladoImporte; string vVersion;
            string excep = ""; string vTipoRelacon; string vCfdiRelacionado;
            try
            {
                Comprobante vcfdi;
                XmlDocument vxml = new XmlDocument();
                vxml.Load(@fileName);
                EscribeLog("Procesando Archivo a DB: " + fileName);

                XmlSerializer serial = new XmlSerializer(typeof(Comprobante));
                using (StreamReader reader = new StreamReader(@fileName))
                {

                    //status.Items[0].Text = "Procesando " + fileName;
                    Application.DoEvents();
                    SplashScreenManager.Default.SetWaitFormDescription("Procesando " + fileName);
                    Application.DoEvents();
                    vcfdi = (Comprobante)serial.Deserialize(reader);
                    vRFCEmisor = vcfdi.Emisor.Rfc;
                    vNombreEmisor = vcfdi.Emisor.Nombre;
                    vRegimenFiscal = vcfdi.Emisor.RegimenFiscal;
                    vRFCReceptor = vcfdi.Receptor.Rfc;

                    if (String.IsNullOrEmpty(vcfdi.Receptor.Nombre))
                        vNombreReceptor = "";
                    else
                        vNombreReceptor = vcfdi.Receptor.Nombre;
                    vUsoCFDI = vcfdi.Receptor.UsoCFDI;
                    vTipoDeComprobante = vcfdi.TipoDeComprobante;
                    if (String.IsNullOrEmpty(vcfdi.Serie))
                        vSerie = "";
                    else
                        vSerie = vcfdi.Serie;
                    if (String.IsNullOrEmpty(vcfdi.Folio))
                        vFolio = "";
                    else
                        vFolio = vcfdi.Folio;
                    vFecha = vcfdi.Fecha.ToString();
                    if (String.IsNullOrEmpty(vcfdi.FormaPago))
                        vFormaPago = "";
                    else
                        vFormaPago = vcfdi.FormaPago;
                    vSubtotal = Convert.ToDouble(vcfdi.SubTotal);
                    vTotal = Convert.ToDouble(vcfdi.Total.ToString());
                    vMoneda = vcfdi.Moneda;
                    vTipoCambio = Convert.ToDouble(vcfdi.TipoCambio.ToString());
                    if (String.IsNullOrEmpty(vcfdi.MetodoPago))
                        vMetodoPago = "";
                    else
                        vMetodoPago = vcfdi.MetodoPago;
                    vLugarExpedicion = vcfdi.LugarExpedicion;

                    //vTipoRelacon = vcfdi.CfdiRelacionados.TipoRelacion;
                    //vCfdiRelacionado = vcfdi.CfdiRelacionados.CfdiRelacionado;
                    if (vNombreReceptor == "" && vRFCReceptor == "XAXX010101000")
                        vNombreReceptor = "PUBLICO EN GENERAL";

                    foreach (var vComplemento in vcfdi.Complemento)
                    {
                        foreach (var vTimbre in vComplemento.Any)
                            if (vTimbre.Name.Contains("TimbreFiscalDigital"))
                            {
                                XmlSerializer serialComplemento = new XmlSerializer(typeof(TimbreFiscalDigital));
                                using (var readerComplemento = new StringReader(vTimbre.OuterXml))
                                {
                                    vcfdi.TimbreFiscalDigital = (TimbreFiscalDigital)serialComplemento.Deserialize(readerComplemento);
                                }
                            }
                    }

                    vUUID = vcfdi.TimbreFiscalDigital.UUID;
                    vFechaTimbrado = vcfdi.TimbreFiscalDigital.FechaTimbrado.ToString();

                    vIDCFDI = Negocio.CNegocio.PCFDI(out excep, 2, vRFCEmisor, vNombreEmisor, vRegimenFiscal, vRFCReceptor, vNombreReceptor, vUsoCFDI, vSerie,
                        vFolio, vFecha, vFormaPago, vSubtotal, vTotal, vMoneda, vTipoCambio, vTipoDeComprobante, vMetodoPago, vLugarExpedicion, vUUID, vFechaTimbrado, "", "", "", 0, Application.StartupPath + "\\CFDI\\Procesados\\" + Path.GetFileName(@fileName), vxml.OuterXml);
                    Application.DoEvents();

                    foreach (var vConceptos in vcfdi.Conceptos)
                    {
                        vIDConcepto = 0;
                        vClaveProdServ = vConceptos.ClaveProdServ;
                        if (String.IsNullOrEmpty(vConceptos.NoIdentificacion))
                            vNoIdentificacion = "";
                        else
                            vNoIdentificacion = vConceptos.NoIdentificacion;
                        vCantidad = Convert.ToDouble(vConceptos.Cantidad);
                        vDescripcion = vConceptos.Descripcion;
                        vValorUnitario = Convert.ToDouble(vConceptos.ValorUnitario);
                        vImporte = Convert.ToDouble(vConceptos.Importe);
                        vClaveUnidad = vConceptos.ClaveUnidad;
                        if (vIDCFDI > 0)
                        {
                            vIDConcepto = Negocio.CNegocio.PCFDIConceptos(out excep, 2, 0, vIDCFDI, vClaveProdServ, vNoIdentificacion, vCantidad, vClaveUnidad, vDescripcion, vValorUnitario, vImporte);
                            if (vConceptos.Impuestos != null)
                            {
                                foreach (var vImpuestos in vConceptos.Impuestos.Traslados)
                                {
                                    vTrasladoBase = Convert.ToDouble(vImpuestos.Base);
                                    vTrasladoImporte = Convert.ToDouble(vImpuestos.Importe);
                                    vTrasladoImpuesto = vImpuestos.Impuesto;
                                    vTrasladoTasaOCuota = Convert.ToDouble(vImpuestos.TasaOCuota);
                                    vTrasladoTipoFactor = vImpuestos.TipoFactor;
                                    vIDImpuesto = Negocio.CNegocio.PCFDIImpuestos(out excep, 2, vIDConcepto, vTrasladoBase, vTrasladoImporte, vTrasladoImpuesto, vTrasladoTasaOCuota, vTrasladoTipoFactor);
                                }
                            }
                        }
                        Application.DoEvents();
                    }
                    Application.DoEvents();
                }
                MoverArchivo(fileName, "\\CFDI\\Procesados\\XML");
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message + " " + fileName);
                SplashScreenManager.Default.SetWaitFormDescription("Error: Al Guardar Datos CFDI; " + fileName + Ex.Message);
                //MessageBox.Show("Error: Al Guardar Datos CFDI; " + Ex.Message, "SGTPOS", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
            Application.DoEvents();
        }

        public static Int64 PCFDIConceptos(out string excep, int vOpcion, Int64 vIDConcepto, Int64 vIDCFDI, string vClaveProdServ, string vNoIdentificacion, double vCantidad, string vClaveUnidad,
            string vDescripcion, double vValorUnitario, double vImporte)
        {
            DataTable tConcepto;
            Int64 vID = 0;
            excep = "";
            tConcepto = Datos.AccesoDatos.PCFDIConceptos(out excep, vOpcion, vIDConcepto, vIDCFDI, vClaveProdServ, vNoIdentificacion, vCantidad, vClaveUnidad, vDescripcion, vValorUnitario, vImporte);
            foreach (DataRow row in tConcepto.Rows)
            {
                vID = Convert.ToInt64(row[0].ToString());
            }
            return vID;
        }

        public static DataTable BuscaConceptos(out string excep, int vOpcion, Int64 vIDConcepto, Int64 vIDCFDI, string vClaveProdServ, string vNoIdentificacion, double vCantidad, string vClaveUnidad,
            string vDescripcion, double vValorUnitario, double vImporte)
        {
            excep = "";
            return Datos.AccesoDatos.PCFDIConceptos(out excep, vOpcion, vIDConcepto, vIDCFDI, vClaveProdServ, vNoIdentificacion, vCantidad, vClaveUnidad, vDescripcion, vValorUnitario, vImporte);
        }


        public static DataTable PSolicitud


            (out string except, int vOpcion, Int64 vIdSolicitud, string vSolicitud, string vEstatus, DateTime vFecha, int vEstadoSolicitud, string vIdPaquete, string vEstadoDescarga, int vAuto, string vTipo, string vBusqueda, string vTipoDescarga)
        {
            except = "";
            return Datos.AccesoDatos.PSolicitud(out except, vOpcion, vIdSolicitud, vSolicitud, vEstatus, vFecha, vEstadoSolicitud, vIdPaquete, vEstadoDescarga, vAuto, vTipo, vBusqueda, vTipoDescarga);
        }

        public static Int64 PCFDIImpuestos(out string excep, int vOpcion, Int64 vIDConcepto, double vBase, double vImporte, string vImpuesto, double vTasaOCuota, string vTipoFactor)
        {
            DataTable tImpuesto;
            Int64 vID = 0;
            excep = "";
            tImpuesto = Datos.AccesoDatos.PCFDImpuestos(out excep, vOpcion, vIDConcepto, vBase, vImporte, vImpuesto, vTasaOCuota, vTipoFactor);
            foreach (DataRow row in tImpuesto.Rows)
            {
                vID = Convert.ToInt64(row[0].ToString());
            }
            return vID;
        }

        public static Int64 PCFDI(out string excep, int vOpcion, string vRFCEmisor, string vNombreEmisor, string vRegimenFiscal, string vRFCReceptor, string vNombreReceptor, string vUsoCFDI, string vSerie, string vFolio, string vFecha,
           string vFormaPago, double vSubtotal, double vTotal, string vMoneda, double vTipoCambio, string vTipoDeComprobante, string vMetodoPago, string vLugarExpedicion, string vUUID, string vFechaTimbrado, string vRFC, string vFechaIni, string vFechaFin, Int64 vIDCFDI, string vArchivo, string vXml)
        {
            DataTable tCDFI;
            Int64 vID = 0;
            excep = "";
            tCDFI = Datos.AccesoDatos.PCFDI(out excep, vOpcion, vRFCEmisor, vNombreEmisor, vRegimenFiscal, vRFCReceptor, vNombreReceptor, vUsoCFDI, vSerie, vFolio, vFecha, vFormaPago, vSubtotal, vTotal, vMoneda, vTipoCambio, vTipoDeComprobante, vMetodoPago, vLugarExpedicion, vUUID, vFechaTimbrado, vRFC, vFechaIni, vFechaFin, vIDCFDI, vArchivo, vXml);
            foreach (DataRow row in tCDFI.Rows)
            {
                vID = Convert.ToInt64(row[0].ToString());
            }
            return vID;
        }

        public static DataTable GetXML(out string excep, int vOpcion, string vRFCEmisor, string vNombreEmisor, string vRegimenFiscal, string vRFCReceptor, string vNombreReceptor, string vUsoCFDI, string vSerie, string vFolio, string vFecha,
           string vFormaPago, double vSubtotal, double vTotal, string vMoneda, double vTipoCambio, string vTipoDeComprobante, string vMetodoPago, string vLugarExpedicion, string vUUID, string vFechaTimbrado, string vRFC, string vFechaIni, string vFechaFin, Int64 vIDCFDI, string vArchivo, string vXml)
        {
            excep = "";
            return Datos.AccesoDatos.PCFDI(out excep, vOpcion, vRFCEmisor, vNombreEmisor, vRegimenFiscal, vRFCReceptor, vNombreReceptor, vUsoCFDI, vSerie, vFolio, vFecha, vFormaPago, vSubtotal, vTotal, vMoneda, vTipoCambio, vTipoDeComprobante, vMetodoPago, vLugarExpedicion, vUUID, vFechaTimbrado, vRFC, vFechaIni, vFechaFin, vIDCFDI, vArchivo, vXml);
        }


        public static DataTable PCargaDatos(out string except, string vTabla, string vRuta)
        {
            except = "";
            return Datos.AccesoDatos.PCargaDatos(out except, vTabla, vRuta);
        }

        public static void MoverArchivo(string sourceFile, string destinationFile)
        {
            string destino = Application.StartupPath + destinationFile + "\\" + Path.GetFileName(sourceFile);
            Creacarpeta(Application.StartupPath + destinationFile);
            //if (File.Exists(destinationFile))
            // File.Delete(destinationFile);
            if (File.Exists(destino))
                File.Delete(destino);
            System.IO.File.Move(sourceFile, destino);
        }

        public static DataTable BuscaCDFI(out string excep, int vOpcion, string vRFCEmisor, string vNombreEmisor, string vRegimenFiscal, string vRFCReceptor, string vNombreReceptor, string vUsoCFDI, string vSerie, string vFolio, string vFecha,
           string vFormaPago, double vSubtotal, double vTotal, string vMoneda, double vTipoCambio, string vTipoDeComprobante, string vMetodoPago, string vLugarExpedicion, string vUUID, string vFechaTimbrado, string vRFC, string vFechaIni, string vFechaFin, Int64 vIDCFDI, string vArchivo)
        {
            excep = "";
            return Datos.AccesoDatos.PCFDI(out excep, vOpcion, vRFCEmisor, vNombreEmisor, vRegimenFiscal, vRFCReceptor, vNombreReceptor, vUsoCFDI, vSerie, vFolio, vFecha, vFormaPago, vSubtotal, vTotal, vMoneda, vTipoCambio, vTipoDeComprobante, vMetodoPago, vLugarExpedicion, vUUID, vFechaTimbrado, vRFC, vFechaIni, vFechaFin, vIDCFDI, vArchivo, "");
        }

        public static string GetXml(Int64 vIDCFDI, out string vUUID)
        {
            string vxml = "", except;
            vUUID = "";
            try
            {
                DataTable vDatos = Datos.AccesoDatos.PCFDI(out except, 4, "", "", "", "", "", "", "", "", "", "", 0, 0, "", 0, "", "", "", "", "", "", "", "", vIDCFDI, "", "");
                foreach (DataRow row in vDatos.Rows)
                {
                    vxml = row["xml"].ToString();
                    vUUID = row["UUID"].ToString();
                }
                return vxml;
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message);
                return "";
            }
        }

        public static DataTable SQSP_BUSCACFDISSP(out string except, string vRfc, string vNombre, string vSerie, string vFolio, string vFechaInicio, string vFechaFin)
        {
            except = "";
            return Datos.AccesoDatos.PBUSCACFDISSP(out except, vRfc, vNombre, vSerie, vFolio, vFechaInicio, vFechaFin);
        }

        public static bool GeneraPDf(string vruta, string vUUDI)
        {
            try
            {
                Negocio.CNegocio.Creacarpeta(@Application.StartupPath + "\\PDF\\");
                Negocio.CNegocio.EscribeLog("Generando PDF " + vruta);
                Comprobante vcfdi;
                byte[] byteArray = Encoding.ASCII.GetBytes(vruta);
                MemoryStream stream = new MemoryStream(byteArray);
                XmlSerializer serial = new XmlSerializer(typeof(Comprobante));
                using (StreamReader reader = new StreamReader(stream))
                {
                    vcfdi = (Comprobante)serial.Deserialize(reader);
                    foreach (var vComplemento in vcfdi.Complemento)
                    {
                        foreach (var vTimbre in vComplemento.Any)
                            if (vTimbre.Name.Contains("TimbreFiscalDigital"))
                            {
                                XmlSerializer serialComplemento = new XmlSerializer(typeof(TimbreFiscalDigital));
                                using (var readerComplemento = new StringReader(vTimbre.OuterXml))
                                {
                                    vcfdi.TimbreFiscalDigital = (TimbreFiscalDigital)serialComplemento.Deserialize(readerComplemento);
                                }
                            }
                        Application.DoEvents();
                    }
                }

                Application.DoEvents();
                string resultHtml = RazorEngine.Razor.Parse(GetStringOfFile(Application.StartupPath + "/plantilla.html"), vcfdi);
                File.WriteAllText(Application.StartupPath + "\\tem.html", resultHtml);
                string pdf = @Application.StartupPath + "\\wkhtmltopdf.exe";

                ProcessStartInfo vproceso = new ProcessStartInfo();
                vproceso.UseShellExecute = false;
                vproceso.FileName = pdf;
                vproceso.Arguments = "tem.html " + vUUDI + ".pdf";
                vproceso.CreateNoWindow = true;
                vproceso.WindowStyle = ProcessWindowStyle.Hidden;
                using (Process process = Process.Start(vproceso))
                {
                    process.WaitForExit();
                }
                Application.DoEvents();
                MoverArchivo(@Application.StartupPath + "\\" + vUUDI + ".pdf", "\\PDF\\");
                System.Diagnostics.Process.Start(@Application.StartupPath + "\\" + "\\PDF\\" + vUUDI + ".pdf");
                System.IO.File.Delete("tem.html");
                Application.DoEvents();
                return true;
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                Application.DoEvents();
                return false;
            }
        }

        private static string GetStringOfFile(string pathFile)
        {
            string contenido = File.ReadAllText(pathFile);
            return contenido;
        }

        public static bool AbrirArchivo(string filename, out string verror)
        {
            verror = "";
            try
            {
                Negocio.CNegocio.EscribeLog("Abriendo Archivo " + filename);
                System.Diagnostics.Process.Start(filename);
                return true;
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
                verror = Ex.Message;
                return false;
            }
        }

        public static bool DescargaSAT(string RFCEmisor, string RFCReceptor, string Finicio, string FFin, TextBox Memo, WaitForm panel, int vAuto, string vTipo, string vBusqueda)
        {
            if (GeneraSolicitud(RFCEmisor, RFCReceptor, Finicio, FFin, Memo, panel, vAuto, vTipo, vBusqueda, "CFDI"))
            {
                if (GeneraSolicitud(RFCEmisor, RFCReceptor, Finicio, FFin, Memo, panel, vAuto, vTipo, vBusqueda, "Metadata"))
                    return true;
                else
                {
                    Memo.Text += DateTime.Now + " " + "Error Al Validar Solicitud Metadata" + "\r\n";
                    return false;
                }
            }
            else
            {
                Memo.Text += DateTime.Now + " " + "Error Al Validar Solicitud CFDI" + "\r\n";
                return false;
            }
        }

        public static bool GeneraSolicitud(string RFCEmisor, string RFCReceptor, string Finicio, string FFin, TextBox Memo, WaitForm panel, int vAuto, string vTipo, string vBusqueda, string vTipoDescarga)
        {
            EscribeLog("Generaando Solicitud en SAT");
            string autorization = GetAutorization(Memo, panel);
            string vCodEstatus, vMensaje, except;
            bool vResult;
            if (autorization == "FALSE")
            {
                Application.DoEvents();
                return false;
            }
            else
            {
                Memo.Text += DateTime.Now + " " + "Generando Solicitud" + "\r\n";
                Application.DoEvents();
                SplashScreenManager.Default.SetWaitFormDescription("Generando Solicitud");
                Application.DoEvents();
                string solicitud = SAT.Logica.GetSolicitud(autorization, RFCEmisor, RFCReceptor, Finicio, FFin, out vResult, out vCodEstatus, out vMensaje, vTipoDescarga);
                EscribeLog("Solicitud;" + solicitud + ", " + vCodEstatus + ", " + vMensaje);
                PSolicitud(out except, 2, 0, solicitud, vCodEstatus, DateTime.Now, 0, "", "", vAuto, vTipo, vBusqueda, vTipoDescarga);
                Memo.Text += DateTime.Now + " " + "Solicitud: " + solicitud + "\r\n";
                Memo.Text += DateTime.Now + " " + "Codigo de Estatus: " + vCodEstatus + "\r\n";
                Memo.Text += DateTime.Now + " " + "Mensaje: " + vMensaje + "\r\n";
                if (vResult)
                    return true;
                else
                    return false;
            }
        }

        public static bool ValidadSolicitud(string RFCEmisor, string RFCReceptor, string Finicio, string FFin, TextBox Memo, WaitForm panel, String vSolicitud, Int64 vIdSolicitud)
        {
            string vNumeroCFDI, vCodEstatus, vMensaje, vEstadoSol, except;
            bool vResult;
            string autorization = GetAutorization(Memo, panel);
            if (autorization == "FALSE")
            {
                Application.DoEvents();
                return false;
            }
            else
            {
                SplashScreenManager.Default.SetWaitFormDescription("Procesando Solicitud");
                Application.DoEvents();
                string idPaquete = SAT.Logica.ValidaSolicitud(autorization, vSolicitud, RFCReceptor, out vResult, out vNumeroCFDI, out vCodEstatus, out vEstadoSol, out vMensaje);
                EscribeLog("Validando solicitud;" + vSolicitud + ", " + idPaquete + ", " + vCodEstatus + ", " + vMensaje + ", " + vNumeroCFDI);
                PSolicitud(out except, 3, vIdSolicitud, "", vCodEstatus, DateTime.Now, Convert.ToInt32(vEstadoSol), idPaquete, vMensaje, 0, "", "", "");
                Memo.Text += DateTime.Now + " " + "Codigo de Estatus " + vCodEstatus + "\r\n";
                Memo.Text += DateTime.Now + " " + "Estado de Solicitud " + vEstadoSol + "\r\n";
                Memo.Text += DateTime.Now + " " + "Mensaje " + vMensaje + "\r\n";
                Memo.Text += DateTime.Now + " " + "CFDI Encontrados " + vNumeroCFDI + "\r\n";
                Memo.Text += DateTime.Now + " " + "IdPaquete " + idPaquete + "\r\n";
                if (vResult)
                    return true;
                else
                    return false;
            }
        }

        public static bool DescargaArchivos(TextBox Memo, WaitForm panel, string IdPaquete, string RFCReceptor, Int64 vIdSolicitud)
        {
            bool vResult;
            string autorization = GetAutorization(Memo, panel);
            string except;
            if (autorization == "FALSE")
            {
                Application.DoEvents();
                return false;
            }
            else
            {
                SplashScreenManager.Default.SetWaitFormDescription("Buscando Paquete");
                if (!string.IsNullOrEmpty(IdPaquete))
                {
                    Application.DoEvents();
                    string descargaResponse = SAT.Logica.DescargaSolicitud(autorization, IdPaquete, RFCReceptor, out vResult);
                    PSolicitud(out except, 3, vIdSolicitud, "", "", DateTime.Now, 10, "", "", 0, "", "", "");
                    Application.DoEvents();
                    SplashScreenManager.Default.SetWaitFormDescription("Descargando Archivos");
                    Application.DoEvents();
                    Memo.Text += System.DateTime.Now + " " + "Descargando Paquete " + IdPaquete + "\r\n";
                    if (SAT.Logica.DescargaArchivos(IdPaquete, descargaResponse))
                    {
                        EscribeLog("Descargando Complea Archivos");
                        Memo.Text += DateTime.Now + " " + "Descarga Completa " + "\r\n";
                        SplashScreenManager.Default.SetWaitFormDescription("Descomprimiendo Archivos");
                        try
                        {
                            string vRutaOrigen = @Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Descargados\\" + IdPaquete + ".zip";
                            DescomprimirArchivo(vRutaOrigen, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Descargados\\" + IdPaquete);
                            MoverArchivo(vRutaOrigen, "\\CFDI\\Procesados\\ZIP");
                            RecoreCarpeta("\\Descargados\\" + IdPaquete, panel);
                            RecoreCarpetaMeta("\\Descargados\\" + IdPaquete, panel);
                            System.IO.Directory.Delete(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Descargados\\" + IdPaquete);
                        }
                        catch (Exception Ex)
                        {
                            Memo.Text += DateTime.Now + " " + "Error al Descomprimir " + "\r\n";
                        }
                    }
                    else
                        Memo.Text += System.DateTime.Now + " " + "Error en Descarga " + "\r\n";
                }
                Application.DoEvents();
                return true;
            }
        }

        public static string GetAutorization(TextBox Memo, WaitForm panel)
        {
            bool vResult;
            Application.DoEvents();
            string token = SAT.Logica.GetToken(SAT.Logica.certificate, out vResult);
            if (vResult)
            {
                string autorization = SAT.Logica.GetAutorization(token);
                Application.DoEvents();
                SplashScreenManager.Default.SetWaitFormDescription("Creando Autentificación");
                Memo.Text += DateTime.Now + " " + "Creando Autentificacion" + "\r\n";
                //EscribeLog("Token: " + token, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                return autorization;
            }
            return "FALSE";
        }

        public static void MonitorSolicitud(TextBox Memo, WaitForm panel)
        {
            string excep = null;
            Int64 vIdSolicitud = 0;
            int vEstado = 0;
            string UUID = null;
            try
            {
                DataTable tDatos = PSolicitud(out excep, 4, 0, "", "", System.DateTime.Now, 0, "", "", 0, "", "", "");
                foreach (DataRow row in tDatos.Rows)
                {
                    vIdSolicitud = Convert.ToInt64(row["IdSolicitud"].ToString());
                    vEstado = Convert.ToInt32(row["Estado"].ToString());
                    UUID = row[0].ToString();
                }
                switch (vEstado)
                {
                    case 0: ValidadSolicitud("SQX981027RY5", "SQX981027RY5", "", "", Memo, panel, UUID, vIdSolicitud); break;
                    case 1: ValidadSolicitud("SQX981027RY5", "SQX981027RY5", "", "", Memo, panel, UUID, vIdSolicitud); break;
                    case 2: ValidadSolicitud("SQX981027RY5", "SQX981027RY5", "", "", Memo, panel, UUID, vIdSolicitud); break;
                    case 3: DescargaArchivos(Memo, panel, UUID, "SQX981027RY5", vIdSolicitud); break;
                }
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message);
            }
        }

        public static bool DescomprimirArchivo(string vRuta, string vDestino)
        {
            try
            {
                EscribeLog("Descomprimiendo " + vRuta);
                ZipFile.ExtractToDirectory(vRuta, vDestino);
                return true;
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message);
                return false;
            }
        }

        public static bool SaveFile(string vTexto, string vRuta, string vNombre)
        {
            try
            {
                Creacarpeta(vRuta);
                using (StreamWriter sw = new StreamWriter(vRuta + vNombre))
                {
                    sw.WriteLine(vTexto);
                }
                return true;
            }
            catch (Exception Ex)
            {
                EscribeLog(Ex.Message);
                return false;
            }
        }

        public static void simulaProceso(TextBox Memo, WaitForm panel, string IdPaquete)
        {
            try
            {
                string vRutaOrigen = @Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Descargados\\" + IdPaquete + ".zip";
                DescomprimirArchivo(vRutaOrigen, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Descargados\\" + IdPaquete);
                MoverArchivo(vRutaOrigen, "\\CFDI\\Procesados\\ZIP");
                RecoreCarpeta("\\Descargados\\" + IdPaquete, panel);
                System.IO.Directory.Delete(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Descargados\\" + IdPaquete);
            }
            catch (Exception Ex)
            {
                Memo.Text += DateTime.Now + " " + "Error al Descomprimir " +Ex.Message+ "\r\n";
            }
        }


        public static DataTable PMetaData(out string except, int vOpcion, string vRfcEmisor, string vNombreEmisor, string vRfcReceptor, string vNombreReceptor, string vRfcPac, string vFechaEmision,
        string vFechaCertificacionSat, string vMonto, string vEfectoComprobante, string vEstatus, string vFechaCancelacion, string vUUID)
        {
            except = "";
            return Datos.AccesoDatos.PMetaData(out except, vOpcion, vRfcEmisor, vNombreEmisor, vRfcReceptor, vNombreReceptor, vRfcPac, vFechaEmision, vFechaCertificacionSat, vMonto, vEfectoComprobante, vEstatus, vFechaCancelacion, vUUID);
        }


        public void prueba()
        {

        }
    }
}
