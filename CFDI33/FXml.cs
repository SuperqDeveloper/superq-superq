﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using System.IO;
using System.Reflection;

namespace CFDI33
{
    public partial class FXml : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public string vIDCFDI;
        public FXml()
        {
            InitializeComponent();
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        private void btn_Guadar_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Negocio.CNegocio.SaveFile(wbXML.Document.ToString(), Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\XML\\",  vIDCFDI+".xml");
            }
            catch (Exception Ex)
            {
                Negocio.CNegocio.EscribeLog(Ex.Message);
            }
        }
    }
}