﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.IO;

namespace Datos
{
    class Configuracion
    {
        private static SqlDataAdapter _DataAdapter;
        private static DataTable _DataTable;
        private static SqlConnection _Conexion;
        private static SqlCommand _Comando;
        private static string _CadenaConexion;
        private static DataSet _DataSet;

        // conexion a base de Datos

        public static void EscribeLog(string vLinea, string ruta)
        {
            try
            {
                string file;
                file = ruta + "\\Log\\" + Getfecha() + ".log";
                Creacarpeta(ruta + "\\Log");
                using (StreamWriter write = new StreamWriter(file, true))
                {
                    write.WriteLine(vLinea + ";" + Gethorafecha() + ";");
                    write.Close();
                }
            }
            catch (Exception Ex)
            {

                EscribeLog(Ex.Message, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                //MessageBox.Show("Error: Al Buscar CFDI'S" + " " + Ex.Message, "SUPERQ", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }

        public static string Getfecha()
        {
            return (DateTime.Now.ToString("ddMMyyyy"));
        }

        public static string Gethorafecha()
        {
            return DateTime.Now.ToString("ddMMyyyy HH:mm:ss");
        }

        public static void Creacarpeta(string ruta)
        {
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
        }


        public static string GetCadenaConexion()
        {

            string vServidor, vCatalogo, vUser, vPassword;
            string cadena = "";
            vServidor = "isypos2\\isypos";//getServidor();
            vCatalogo = "CFDI33";
            vUser = "isypos";
            vPassword = "isypos";
            cadena = "server =" + vServidor + "; initial catalog = " + vCatalogo + "; user id = " + vUser + "; password = " + vPassword;
            //MessageBox.Show(cadena);
            return (cadena);
        }

        public static string GetServidor()
        {
            string pcName;
            pcName = GetPcName();
            try
            {
                pcName = pcName.Substring(pcName.IndexOf("-") + 1, 2);
            }
            catch
            {

            }
            if (pcName == "01")
                return ("localhost\\isypos");
            else
                // return ("caja1");

                //pcName = pcName + "-01\\isypos";
                return ("sq0666-01\\isypos");
        }

        public static string GetPcName()
        {
            return System.Environment.MachineName;
        }

        // escribe Log
        public bool ExisteEncript(string ruta)
        {
            try
            {
                string file;
                file = ruta;
                if (System.IO.File.Exists(file))
                    return (true);
                else
                    return (false);
            }
            catch
            {
                return (false);
            }
        }

        // ejecutar procedimientos

        public static SqlTransaction CrearTransaction(SqlConnection pConexion)
        {

            SqlTransaction _Transaction = null;

            _Transaction = pConexion.BeginTransaction();
            return _Transaction;
        }

        public static SqlCommand CrearComando()
        {
            _CadenaConexion = Configuracion.GetCadenaConexion();
            //MessageBox.Show(_CadenaConexion);
            _Conexion = new SqlConnection();
            _Conexion.ConnectionString = _CadenaConexion;

            _Comando = new SqlCommand();
            _Comando = _Conexion.CreateCommand();
            _Comando.CommandType = CommandType.Text;
            return _Comando;
        }

        public static SqlCommand CrearComandoProc(string pNombreProcedure)
        {
            _CadenaConexion = Configuracion.GetCadenaConexion();
            _Conexion = new SqlConnection(_CadenaConexion);
            _Comando = new SqlCommand(pNombreProcedure, _Conexion);
            _Comando.CommandTimeout = 0;
            _Comando.CommandType = CommandType.StoredProcedure;
            return _Comando;
        }

        public static Boolean EjecutarComando(SqlCommand pComando)
        {
            try
            {
                pComando.Connection.Open();
                pComando.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                EscribeLog(ex.Message, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                return false;
                throw ex;
            }
            finally
            {
                pComando.Connection.Dispose();
                pComando.Connection.Close();
            }
        }
        public static DataTable EjecutarComandoSelect(SqlCommand pComando)
        {
            _DataTable = new DataTable();
            try
            {
                pComando.Connection.Open();
                _DataAdapter = new SqlDataAdapter();
                _DataAdapter.SelectCommand = pComando;
                _DataAdapter.Fill(_DataTable);
                return _DataTable;
            }
            catch (Exception ex)
            {
                EscribeLog(ex.Message, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                throw new System.ArgumentOutOfRangeException("", ex);
            }
            finally
            {
                pComando.Connection.Close();
            }
        }
        public static bool EjecutarTransaction(SqlCommand pComando, SqlTransaction pTransaction)
        {
            try
            {
                //pComando.Connection.Open();
                pComando.ExecuteNonQuery();
                //pTransaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                EscribeLog(ex.Message, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                throw ex;
            }
            finally { }
        }
        public static DataTable EjecutarProcedure(SqlCommand pComando, SqlParameter[] pParametros, out string excep)
        {
            excep = "";
            try
            {
                pComando.Parameters.AddRange(pParametros);
                _DataAdapter = new SqlDataAdapter(pComando);
                _DataTable = new DataTable();
                _DataAdapter.Fill(_DataTable);
                return _DataTable;
            }
            catch (Exception ex)
            {
                //throw ex;
                excep = ex.Message;
                EscribeLog(ex.Message, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                //MessageBox.Show("Error: " + ex.Message, "Error al EjecutarProcedure...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            finally
            {
                excep = "";
            }
        }

        public static DataSet EjecutarProcedureDS(SqlCommand pComando, SqlParameter[] pParametros, out string excep)
        {
            excep = "";
            try
            {
                pComando.Parameters.AddRange(pParametros);
                _DataAdapter = new SqlDataAdapter(pComando);
                _DataSet = new DataSet();
                _DataAdapter.Fill(_DataSet);
                return _DataSet;
            }
            catch (Exception ex)
            {
                EscribeLog(ex.Message, Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                excep = ex.Message;
                throw ex;
            }
            finally { }
        }
    }
}
